package mapper;

import constants.SqlConstant;
import constants.SystemLibConstant;
import utils.SystemMetastoreUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * ClassName:MainMapper
 * Package:mapper
 * Description:
 *
 * @Author: thechen
 * @Create: 2023/11/5 - 13:51
 */
public class MainMapper {
	private MainMapper() {
	}

	//更新用户的项目权限表
	public static void updateUserPermission(List<List<Object>> paramsList, int insertCount) throws SQLException, IOException {
		String updateSql = String.format(SqlConstant.database_change + "delete from `%s` where `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.PROJECT_PERMISSION_TBL,
				SystemLibConstant.PROJECT_PERMISSION_PROJECT_NAME);

		for (int i = 0; i < insertCount; i++) {
			updateSql += String.format("insert into `%s` values (?,?);",
					SystemLibConstant.PROJECT_PERMISSION_TBL);
		}

		SystemMetastoreUtil.processSql(updateSql, paramsList);
	}

	//根据项目查询用户的项目权限表
	public static List<LinkedHashMap<Object, Object>> queryUserPermissionByProject(List<List<Object>> paramsList) throws SQLException, IOException {
		String querySql = String.format("use `%s`;select * from `%s` where `%s` = ?;", SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.PROJECT_PERMISSION_TBL, SystemLibConstant.PROJECT_PERMISSION_PROJECT_NAME);
		return SystemMetastoreUtil.processSql(querySql, paramsList);
	}

	//查询所有用户信息
	public static List<LinkedHashMap<Object, Object>> queryUserAll() throws SQLException, IOException {
		String querySql = String.format("use `%s`;select * from `%s`;", SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.USERS_TBL);
		return SystemMetastoreUtil.processSql(querySql, null);
	}


	//更新某用户最近登录时间
	public static void updateUserLastLoginTime(List<List<Object>> paramsList) throws SQLException, IOException {
		String alterSql = String.format(SqlConstant.database_change + "update `%s` set `%s` = CURRENT_TIME where `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.USERS_TBL,
				SystemLibConstant.USERS_LAST_LOGIN_TIME,
				SystemLibConstant.USERS_USER_NAME);
		SystemMetastoreUtil.processSql(alterSql,paramsList);
	}

	//插入新的用户
	public static void insertUser(List<List<Object>> paramsList) throws SQLException, IOException {
		String insertSql = String.format(SqlConstant.database_change + "insert into `%s` values (?,?,?,CURRENT_TIME,null);",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.USERS_TBL);
		SystemMetastoreUtil.processSql(insertSql,paramsList);
	}

	//更新某用户密码
	public static void updateUserPass(List<List<Object>> paramsList) throws SQLException, IOException {
		String alterSql = String.format(SqlConstant.database_change + "update `%s` set `%s` = ? where `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.USERS_TBL,
				SystemLibConstant.USERS_PASSWD,
				SystemLibConstant.USERS_USER_NAME);
		SystemMetastoreUtil.processSql(alterSql,paramsList);
	}

	//新建项目
	public static void insertProject(List<List<Object>> paramsList) throws SQLException, IOException {
		String insertSql = String.format(SqlConstant.database_change + "insert into `%s` values (?, ?, ?);",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.PROJECTS_TBL);
		SystemMetastoreUtil.processSql(insertSql,paramsList);
	}

	//更新项目
	public static void updateProject(List<List<Object>> paramsList) throws SQLException, IOException {
		String alterSql = String.format(SqlConstant.database_change + "update `%s` set `%s` = ? where `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.PROJECTS_TBL,
				SystemLibConstant.PROJECTS_POST,
				SystemLibConstant.PROJECTS_PROJECT_NAME);
		SystemMetastoreUtil.processSql(alterSql,paramsList);
	}

	//根据项目查询项目所属人
	public static List<LinkedHashMap<Object, Object>> queryOwnerByProject(List<List<Object>> paramsList) throws SQLException, IOException {
		String querySql = String.format(SqlConstant.database_change + "select * from `%s` where `%s` = ?;", SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.PROJECTS_TBL, SystemLibConstant.PROJECTS_PROJECT_NAME);
		return SystemMetastoreUtil.processSql(querySql, paramsList);
	}

	//查询项目列表
	public static List<LinkedHashMap<Object, Object>> queryProjectAll() throws SQLException, IOException {
		String querySql = String.format(SqlConstant.select_all, SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.PROJECTS_TBL);
		return SystemMetastoreUtil.processSql(querySql, null);
	}

	//删除项目
	public static void deleteProject(List<List<Object>> paramsList) throws SQLException, IOException {
		//删除所有表中与该项目相关的数据
		String deleteSql = String.format(SqlConstant.database_change + "delete from `%s` where `%s` = ?;delete from `%s` where `%s` = ?;delete from `%s` where `%s` = ?;delete from `%s` where `%s` = ?;delete from `%s` where `%s` = ?;delete from `%s` where `%s` = ?;delete from `%s` where `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE,
				SystemLibConstant.PROJECTS_TBL, SystemLibConstant.PROJECTS_PROJECT_NAME,
				SystemLibConstant.PROJECT_PERMISSION_TBL, SystemLibConstant.PROJECT_PERMISSION_PROJECT_NAME,
				SystemLibConstant.MX_TREE_TBL, SystemLibConstant.MX_TREE_PROJECT_NAME,
				SystemLibConstant.MX_DETAIL_TBL, SystemLibConstant.MX_DETAIL_PROJECT_NAME,
				SystemLibConstant.KF_FILE_INFO_TBL, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME,
				SystemLibConstant.TB_JOB_BASE_TBL, SystemLibConstant.TB_JOB_BASE_PROJECT_NAME,
				SystemLibConstant.MX_DETAIL_TBL, SystemLibConstant.MX_DETAIL_PROJECT_NAME);
		SystemMetastoreUtil.processSql(deleteSql,paramsList);
	}
}