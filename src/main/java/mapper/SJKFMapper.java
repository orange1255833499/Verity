package mapper;

import constants.SystemLibConstant;
import utils.SystemMetastoreUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * ClassName:SJKFMapper
 * Package:mapper
 * Description:
 *
 * @Author: thechen
 * @Create: 2023/11/5 - 13:41
 */
public class SJKFMapper {
	private SJKFMapper() {
	}

	//根据项目名称查询文件树表所有信息
	public static List<LinkedHashMap<Object, Object>> queryFileInfoByProject(List<List<Object>> paramsList) throws SQLException, IOException {
		String fileQuerySql = String.format("use `%s`;select * from `%s` where `%s` = ?;", SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.KF_FILE_INFO_TBL, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME);
		return SystemMetastoreUtil.processSql(fileQuerySql, paramsList);
	}

	//新建文件夹、文件
	public static void insertDirectoryOrFile(List<List<Object>> paramsList) throws SQLException, IOException {
		String insertSql = String.format("use `%s`;insert into `%s` values (?, ?, ?, ?, ?);",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.KF_FILE_INFO_TBL);
		SystemMetastoreUtil.processSql(insertSql,paramsList);
	}

	//重命名文件夹
	public static void updateDirectory(List<List<Object>> paramsList, String parentDirect) throws SQLException, IOException {
		String updateSql = String.format("use `%s`;update `%s` set `%s` = ? where `%s` = ? and `%s` = ? and `%s` = ? and `%s` = ?;update `%s` set `%s` = concat(?, substring(`%s`, length(?) + 1)) where `%s` like '%s%%' and `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE,
				SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_PATH_NAME,  SystemLibConstant.KF_FILE_INFO_PATH_NAME,  SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH,  SystemLibConstant.KF_FILE_INFO_FILE_TYPE, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME,
				SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH,  SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH,  SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH,parentDirect,SystemLibConstant.KF_FILE_INFO_PROJECT_NAME
		);
		SystemMetastoreUtil.processSql(updateSql,paramsList);
	}

	//删除文件夹
	public static void deleteDirectory(List<List<Object>> paramsList, String finalSelectedValue) throws SQLException, IOException {
		String deleteSql = String.format("use `%s`;delete from `%s` where (`%s` = ? and `%s` = ? and `%s` = ? and `%s` = ?) or (`%s` like '%s%%' and `%s` = ?) or (`%s` = '%s' and `%s` = ?);",
				SystemLibConstant.SYSTEM_DATABASE,
				SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_PATH_NAME,  SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH, SystemLibConstant.KF_FILE_INFO_FILE_TYPE,SystemLibConstant.KF_FILE_INFO_PROJECT_NAME,
				SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH, finalSelectedValue + '.',SystemLibConstant.KF_FILE_INFO_PROJECT_NAME,
				SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH, finalSelectedValue, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME
		);
		SystemMetastoreUtil.processSql(deleteSql,paramsList);
	}

	//重命名文件
	public static void updateFileName(List<List<Object>> paramsList) throws SQLException, IOException {
		String updateSql = String.format("use `%s`;update `%s` set `%s` = ? where `%s` = ? and `%s` != ? and `%s` = ? and `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE,
				SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_PATH_NAME,  SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH,  SystemLibConstant.KF_FILE_INFO_FILE_TYPE, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME, SystemLibConstant.KF_FILE_INFO_PATH_NAME
		);
		SystemMetastoreUtil.processSql(updateSql,paramsList);
	}

	//保存文件
	public static void updateFileContent(List<List<Object>> paramsList) throws SQLException, IOException {
		String updateSql = String.format("use `%s`;update `%s` set `%s` = ? where `%s` = ? and `%s` = ? and `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_CONTENT, SystemLibConstant.KF_FILE_INFO_PATH_NAME, SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME);
		SystemMetastoreUtil.processSql(updateSql,paramsList);
	}

	//删除文件
	public static void deleteFile(List<List<Object>> paramsList) throws SQLException, IOException {
		String deleteSql =  String.format("use `%s`;delete from `%s` where `%s` = ? and `%s` = ? and `%s` != ? and `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_PATH_NAME,  SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH, SystemLibConstant.KF_FILE_INFO_FILE_TYPE, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME
		);
		SystemMetastoreUtil.processSql(deleteSql,paramsList);
	}

	//查询文件内容
	public static List<LinkedHashMap<Object, Object>> queryFileContent(List<List<Object>> paramsList) throws SQLException, IOException {
		String querySql = String.format("use `%s`;select * from `%s` where `%s` = ? and `%s` = ? and `%s` = ?;",
				SystemLibConstant.SYSTEM_DATABASE, SystemLibConstant.KF_FILE_INFO_TBL,
				SystemLibConstant.KF_FILE_INFO_PATH_NAME,
				SystemLibConstant.KF_FILE_INFO_PARENT_TREE_PATH, SystemLibConstant.KF_FILE_INFO_PROJECT_NAME);
		return SystemMetastoreUtil.processSql(querySql,paramsList);
	}
}