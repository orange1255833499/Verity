package constants;

/**
 * ClassName:SystemLibConstant
 * Package:com.common.constants
 * Description:系统库的库名表名字段名常量
 *
 * @Author: thechen
 * @Create: 2023/7/11 - 11:40
 */
public interface SystemLibConstant {
    String SYSTEM_DATABASE = "verity";
        String USERS_TBL = "USERS";
            String USERS_USER_NAME = "USER_NAME";
            String USERS_PASSWD = "PASSWD";
            String USERS_PHONE_NUM = "PHONE_NUM";
            String USERS_CREATE_TIME = "CREATE_TIME";
            String USERS_LAST_LOGIN_TIME = "LAST_LOGIN_TIME";

        String KF_FILE_INFO_TBL = "KF_FILE_INFO";

            String KF_FILE_INFO_PROJECT_NAME = "PROJECT_NAME";
            String KF_FILE_INFO_PATH_NAME = "TREE_PATH_NAME";
            String KF_FILE_INFO_PARENT_TREE_PATH = "PARENT_TREE_PATH";
            String KF_FILE_INFO_FILE_TYPE = "FILE_TYPE";
            String KF_FILE_INFO_CONTENT = "CONTENT";

        String PROJECTS_TBL = "PROJECTS";
            String PROJECTS_PROJECT_NAME = "PROJECT_NAME";
            String PROJECTS_POST = "PROJECT_POST";
            String PROJECTS_OWNER = "OWNER";

        String MX_TREE_TBL = "MX_TREE";
            String MX_TREE_PROJECT_NAME = "PROJECT_NAME";
            String MX_TREE_TREE_PATH = "TREE_PATH";
            String MX_TREE_TREE_TYPE = "TREE_TYPE";

        String MX_DETAIL_TBL = "MX_DETAIL";
            String MX_DETAIL_PROJECT_NAME = "PROJECT_NAME";
            String MX_DETAIL_MODULE_PATH = "MODULE_PATH";
            String MX_DETAIL_CHI_FIELD_NAME = "CHI_FIELD_NAME";
            String MX_DETAIL_ENG_FIELD_NAME = "ENG_FIELD_NAME";
            String MX_DETAIL_FIELD_DATATYPE = "FIELD_DATATYPE";
            String MX_DETAIL_RES_TABLE_NAME = "RES_TABLE_NAME";
            String MX_DETAIL_RES_CHI_FIELD_NAME = "RES_CHI_FIELD_NAME";
            String MX_DETAIL_RES_ENG_FIELD_NAME = "RES_ENG_FIELD_NAME";
            String MX_DETAIL_ROW_ID = "ROW_ID";

        String PROJECT_PERMISSION_TBL = "PROJECT_PERMISSION";
            String PROJECT_PERMISSION_USER_NAME = "USER_NAME";
            String PROJECT_PERMISSION_PROJECT_NAME = "PROJECT_NAME";
        String TB_JOB_BASE_TBL = "TB_JOB_BASE";
            String TB_JOB_BASE_PROJECT_NAME = "PROJECT_NAME";
            String TB_JOB_BASE_TREE_PATH = "TREE_PATH";
            String TB_JOB_BASE_TREE_PATH_TYPE = "TREE_PATH_TYPE";
            String TB_JOB_BASE_CONTENT = "CONTENT";
            String TB_JOB_BASE_SOURCE_DATA = "SOURCE_DATA";
            String TB_JOB_BASE_TARGET_DATA = "TARGET_DATA";
            String TB_JOB_BASE_SOURCE_BASE = "SOURCE_BASE";
            String TB_JOB_BASE_TARGET_BASE = "TARGET_BASE";
            String TB_JOB_BASE_SOURCE_TABLE = "SOURCE_TABLE";
            String TB_JOB_BASE_TARGET_TABLE = "TARGET_TABLE";
        String TB_JOB_DETAIL_TBL = "TB_JOB_DETAIL";
            String TB_JOB_DETAIL_PROJECT_NAME = "PROJECT_NAME";
            String TB_JOB_DETAIL_TREE_PATH = "TREE_PATH";
            String TB_JOB_DETAIL_SOURCE_FIELD = "SOURCE_FIELD";
            String TB_JOB_DETAIL_TARGET_FIELD = "TARGET_FIELD";
}
